# Accredify Super Basic White Label App
*Updated: Feb 15 2016*  
Developed to test integration of white label API endpoints. 

Only **authorized** apps have access to our white label API, please contact Peter@Accredify.com for more information. 

## Dependencies
- PHP 5.2+
- Composer [Getting Started](https://getcomposer.org/)
- Guzzle HTTP Client (Installed via Composer)
- adoy/PHP-OAuth2 (Installed via Composer)

## Getting Started

**Step 1, Install Dependencies:** Install dependencies by locating the white label mini app AccredifyPHP directory and running the command 
``` composer install```

**Step 2, Set Environment Variables** Locate the AccredifyPHP directory, copy example .env file (.env_example) to .env  ```cp .env_example .env```. 

- Get Keys/Register App
    - [Sandbox Developer Portal](https://developer.sandbox.accredify.com)
    - [Production Developer Portal](https://developer.accredify.com)

- Configure **.env** 
    - Enviroment
        - Testing Sandbox: ```APP_ENV=sandbox```
        - Testing Production ```APP_ENV=production```
    - Public Key ```APP_ID=<YOUR_PUBLIC_KEY>```
    - Private Key ```APP_SECRET=<YOUR_PUBLIC_KEY>```


**Step 3, Host App** Using your favorite LAMP/LEMP stack host the mini app for testing. 

For more info, please review the Documentation.pdf located within the source.