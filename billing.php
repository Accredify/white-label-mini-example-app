<?php
include 'AccredifyPHP/Accredify.php';
use Accredify\API as AccredifyAPI;
$AccredifyAPI = new AccredifyAPI;
$billingDetails = $AccredifyAPI::getBillingDetails();//Signed oAuth2 Request :: Get Billing Details
$billingHistory = $AccredifyAPI::getBillingHistory();//Signed oAuth2 Request :: Get Billing History
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Twitter Bootstrap (Not Required) -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
	<style>body {padding-top: 60px;}</style>

</head>
<body>
	<div class="navbar navbar-inverse navbar-fixed-top"></div>
	<div class="container">
	
	<!-- Previously Billed -->
          <div class="row">
            <div class="col-xs-12">
                <section class="panel">
                      <header class="panel-heading"><i class="fa fa-dollar"></i> Billing History</header>
                      <ul>
                      	<li>Single Verification Status: <strong><?= ($billingDetails['data']['single_verification_on'])? "On" : "Off";?></strong></li>
                      	<li>Credit Card: <strong><?= $billingDetails['data']['card_type'] ?> xxx-xxxx-<?= $billingDetails['data']['last_four'] ?></strong></li>
                      	<li># of Credits: <strong><?= $billingDetails['data']['credits'] ?></strong></li>

                      </ul>
                      <div class="table-responsive">
                        <table class="table table-striped m-b-none" data-ride="datatables">
                          <thead>
                            <tr>
                              <th width="25%">Investor</th>
                              <th width="15%">Amount</th>
                              <th width="15%">Date</th>
                            </tr>
                          </thead>
                          <tbody>
                          	<?php foreach($billingHistory['data'] as $bill):?>
                          		<tr>
                          			<td><?= $bill['fullname'];?></td>
                          			<td>$<?= $bill['amount'];?></td>
                          			<td><?= date('m/d/Y',$bill['billed']);?></td>

                          		</tr>
                          	<?php endforeach;?>
                          </tbody>
                        </table>
                      </div>
                  </section>
            </div>
          </div>
          <!-- //Previously Billed -->
	
			
	</div> <!-- /container -->
	
	<!-- Twitter Bootstrap (Not Required-->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</body>
</html>