/*!
 * Accredify v0.1.0 
 */
(function() {
Number.prototype.formatMoney = function(c, d, t){
	var n = this, 
	    c = isNaN(c = Math.abs(c)) ? 2 : c, 
	    d = d == undefined ? "." : d, 
	    t = t == undefined ? "," : t, 
	    s = n < 0 ? "-" : "", 
	    i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
	    j = (j = i.length) > 3 ? j % 3 : 0;
	   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
 };
 Array.prototype.filterObjects = function(key, value) {
    return this.filter(function(x) { return x[key] === value; })
}
String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

Accredify={

	/*
	*	Dependencies Functions
	*		- Initialize Event Listners, Load Dependencies
	*		- If, Dependencies completed: Initiate Form
	*/
	Dependencies:{
		path: 'https://s3.amazonaws.com/accredify-assets/whitelabel-api/',
		files:{'js':['formater.min.js','parsley.min.js','jquery.maskMoney.min.js'],'css':['parsley.css']},
		check:{'formater_min_js':false,'parsley_min_js':false,'jquery_maskMoney_min_js':false,'parsley_css':false},
		head: null,
		init:function(){
			//Add Event Listners
 			Accredify.Dependencies.head = document.getElementsByTagName("head")[0];    			
			Accredify.Dependencies.head.addEventListener("load", function(event) {
				var loaded , file;
				if (event.target.nodeName === "SCRIPT" ){
					loaded = event.target.getAttribute("src").toString().split("/");
				}else if(event.target.nodeName === "LINK" ){
					loaded = event.target.getAttribute("href").toString().split("/");
				}
				file = loaded[loaded.length - 1].replace(/\./g,'_');					
				Accredify.Dependencies.check[file] = true;//File Loaded If exist
				Accredify.Dependencies.checkDependencies();//Check Dependencies Loaded
				if(file == "signature_pad_min_js"){ Accredify.SignaturePad.init(); }//Init SignaturePad on Load
			}, true);

			//Custom Form Dependencies
			switch(accredify_params.type){
				case 'income_request':
					Accredify.Dependencies.check['signature_pad_min_js'] = false;
					Accredify.Dependencies.files['js'].push('signature_pad.min.js');
				break;				
			}			
			
			//Include JS Files
			$(Accredify.Dependencies.files['js']).each(function(index,file_name){
				Accredify.Dependencies.loadJS(file_name);
			});
			//Include CSS Files
			$(Accredify.Dependencies.files['css']).each(function(index,file_name){
				Accredify.Dependencies.loadCSS(file_name);				
			});
		},
		loadJS:function(file_name){
			var accSrc = document.createElement("script"); accSrc.type = "text/javascript";
			accSrc.async = true;
			accSrc.src = Accredify.Dependencies.path+file_name;
			Accredify.Dependencies.head.appendChild(accSrc); 
		},
		loadCSS:function(file_name){
			var accSrc = document.createElement("link"); accSrc.rel = "stylesheet";
			accSrc.async = true;
			accSrc.href = Accredify.Dependencies.path+file_name;
			Accredify.Dependencies.head.appendChild(accSrc); 
		},
		checkDependencies:function(){
			var fully_loaded = false;
			$.each( Accredify.Dependencies.check, function( key, value ) {
				fully_loaded = value;
				return value;
			});

			if(fully_loaded){ 
				Accredify.init(); 
			}
		}
	},


	/*
	*	Helper Functions
	*		- Initiate Form's On Dependecies load 
	*		- Input Masks
	*		- Form Validation
	*		- Signature Pad (Used on request income form)
	*		- DOM Tools
	*		- Asset Tools
	*/

	//Determins which form to intialize
	init:function(){
		//Ensure Form Initator Params are passed			
		if(accredify_params != undefined && accredify_params.form != undefined && accredify_params.type != undefined){
			//Universal Form Requirments
			Accredify.Validator.init(accredify_params.form);//Validate Form		
			Accredify.addMasks();//Mask Inputs
			Accredify.DOM.init();//Setups Input Listners for Conditional Logic & CTA's
			

			//Custom Form Requirments
			switch(accredify_params.type){
				case 'income_request':
					Accredify.Validator.checkSignature = true;//Require Signature
				break;									
				case 'asset':
					Accredify.Assets.init();
				break;
			}
		}
		else{console.log("Error, Accredify Params Not Set")}
	},

	//Mask Inputs (Formating)
	addMasks:function(){
		$(".accredify_mask_social").mask('999-99-9999');
		$(".accredify_mask_social").focus(function(){$(this).attr("type","text");});
		$(".accredify_mask_social").blur(function(){$(this).attr("type","password");});	
		$(".accredify_mask_date").mask('99/99/9999');			
		$(".accredify_mask_phone").mask('(999) - 999 - 9999');
		$(".accredify_mask_money").maskMoney({'precision':0});
	},


	//Signature Pad Used in Income IRS Requests
	SignaturePad:{
		signaturePadObj:null,

		init:function(){
			Accredify.SignaturePad.signaturePadObj = new SignaturePad(document.querySelector("#accredifyCanvas"));
			$(".clear_signature").click(function(){Accredify.SignaturePad.clear()});
		},

		clear:function(){
			Accredify.SignaturePad.signaturePadObj.clear();//Clears Canvas
		},

		validate:function(){
			$("#accredifyCanvas").css("border-color","none");
			if(Accredify.SignaturePad.signaturePadObj.isEmpty()){
				$("#accredifyCanvas").css("border-color","red");
				return false;					
			}else{
				$(".accredify_attribute_signature").val(Accredify.SignaturePad.signaturePadObj.toDataURL());//Take Signature and push IMG data to hidden field
				return true;					
			}
		}
	},//Signature Pad

	
	//Form Validator
	Validator:{
		parsleyObj:null,//Validator Object
		formObj:null,//Form Object,
		checkSignature:false,//Used on Income Request

		init:function(formObj){
			Accredify.Validator.formObj = $(formObj);
			Accredify.Validator.set(); //Sets Validator
		},

		set:function(){
			Accredify.Validator.parsleyObj = Accredify.Validator.formObj.parsley();
			$(Accredify.Validator.formObj).submit(function(e){
					if( Accredify.Validator.parsleyObj.validate() && (Accredify.Validator.checkSignature == false || ( Accredify.Validator.checkSignature == true && Accredify.SignaturePad.validate() ))){
					return true;
					}
					return false;
			});
		},

		//Used when dynamiclly added form objects to DOM
		reset:function(){
			Accredify.Validator.destroy();
			Accredify.Validator.set();
		},
		destroy:function(){
			Accredify.Validator.parsleyObj.destroy();//Distroy Currnt Parsley Checker		
		}
	},//Validator
	

	//DOM Conditional,  DOM Builder, Themable Elements
	DOM:{
		templates:{},
		init:function(){
			//Add Listners for Conditionals
			$(".accredify_input_container").each(function(i,v){
				var conditional = $(v).attr('data-accredify-conditional');
				if( conditional != undefined && conditional.length > 0){
					Accredify.DOM.setConditional(conditional);
				}
			});

			//Add template to DOM Object for later use.
			$(".accredify_has_template").each(function(i,v){
				Accredify.DOM.templates[$(v).attr('data-accredify-template-key')] =   $.parseJSON($(v).attr('data-accredify-template-value'));					
			});

			//Enable CTA's
			$(".accredify_cta").each(function(i,v){
				$(v).click(function(event){
					event.preventDefault();
					var type = $(this).attr("data-accredify-cta-type");
					var container = $(this).attr("data-accredify-cta-container");
					var template = $(this).attr("data-accredify-cta");

					switch(type){
						case "addAsset":
							Accredify.Assets.add(container,template);
						break;
					}
				});
			});					
			
		},
		
		//Setup Input Listners for change, trigger rules			
		setConditional:function(conditional){
			var rules = $.parseJSON(conditional);
			switch(rules.action){
				case 'addRemove':
					$(rules.listen).change(function(){
						var current_val = $(this).val();
						$(rules.test).each(function(i,v){
							if(v.value == current_val && v.method == "-"){
								Accredify.DOM.clear(rules.target);
							}else if(v.value == current_val && v.method == "+"){
								$(v.dom).each(function(i,inputObj){
									var domElement = Accredify.DOM.inputBuilder(inputObj,true);//Generate DOM Element
									$(rules.target).append(domElement);//Append DOM Element

								});
								Accredify.addMasks();//Mask Inputs
								Accredify.Validator.reset();//Reset Validators to account for new DOM elements								
							}
						});
					});
				break;
			}
		},
	
		clear:function(target){
			$(target).html('');//Clear DOM if conditional requires it
		},
		remove:function(target){
			$(target).remove();//Remove Object from DOM
		},
		inputBuilder:function(inputObj,with_wrapper){
			var domElement="";

			switch(inputObj.input){
				case 'text':
					domElement = (with_wrapper)? Accredify.DOM.elementInputWrapper(inputObj) : Accredify.DOM.elementInput(inputObj);
				break;
				case 'password':
					domElement = (with_wrapper)? Accredify.DOM.elementInputWrapper(inputObj) : Accredify.DOM.elementInput(inputObj);
				break;
				case 'file':
					domElement =  (with_wrapper)? Accredify.DOM.elementInputWrapper(inputObj) : Accredify.DOM.elementInput(inputObj);
				break;					
				case 'select':
					domElement = (with_wrapper)? Accredify.DOM.elementSelectWrapper(inputObj) : Accredify.DOM.elementSelect(inputObj);					
				break;
				case 'hidden':
					domElement = Accredify.DOM.elementInput(inputObj);
				break;					
			}

			return domElement;
		},

		/********
		*
		*	Themable: Feel free to edit any of this markup
		*
		********/
		elementInput:function(inputObj){
			var required = (inputObj.required)? "data-parsley-required='true'" : "data-parsley-required='false'";//Used in Parsley Validation				
			var domElement = "<input type='"+inputObj.input+"' name='"+inputObj.name+"' class='"+inputObj.class+"' placeholder='"+inputObj.placeholder+"' "+required+" >";
			return domElement;
		},

		elementInputWrapper:function(inputObj){
			var domElement = "";
			var required = (inputObj.required)? "data-parsley-required='true'" : "data-parsley-required='false'";//Used in Parsley Validation			

			//Required Class => accredify_input_container
			//Required Attribute => data-accredify-conditional=''			
			domElement += "<div class='col-sm-12 accredify_input_container' data-accredify-conditional=''>";
				domElement += "<label>"+inputObj.label+"</label>";				
				domElement += Accredify.DOM.elementInput(inputObj);
			domElement += "</div>";

			return domElement;
		},
		
		elementSelect:function(inputObj){
			var required = (inputObj.required)? "data-parsley-required='true'" : "data-parsley-required='false'";//Used in Parsley Validation							
			var domElement = "";
			
			domElement += "<select name='"+inputObj.name+"' class='"+inputObj.class+"' "+required+">";
			domElement += "<option value=''>Select an Option</option>";
			console.log(inputObj.options)
			$.each(inputObj.options, function( key, value ) {
				domElement += "<option value='"+key+"'>"+value+"</option>";
			});
			domElement += "</select>";

			return domElement;
		},

		elementSelectWrapper:function(inputObj){
			var domElement = "";
			var required = (inputObj.required)? "data-parsley-required='true'" : "data-parsley-required='false'";//Used in Parsley Validation			

			//Required Class => accredify_input_container
			//Required Attribute => data-accredify-conditional=''			
			domElement += "<div class='col-sm-12 accredify_input_container' data-accredify-conditional=''>";
				domElement += "<label>"+inputObj.label+"</label>";
				domElement += Accredify.DOM.elementSelect(inputObj);
			domElement += "</div>";

			return domElement;
		},			

		elementAssetLiabilityRow:function(singular,current_index,template_index){
			domElement = "";
			domElement += "<tr class='accredify_"+singular+"_"+current_index+" accredify_"+singular+"' data-accredify-"+singular+"='"+current_index+"'>";

			$.each( Accredify.DOM.templates[template_index], function( key, value ) {
				domElement += "<td>";
				domElement += String(Accredify.DOM.inputBuilder(value)).replaceAll("xxx", current_index);
				domElement += "</td>";
			});
			
			domElement += "<td>";
				domElement += "<img class='accredify_"+singular+"_"+current_index+" accredify_"+singular+"_remove' src='https://s3.amazonaws.com/accredify-assets/whitelabel-api/ic_remove.png' style='cursor:pointer; width:20px;' title='Remove'>";
			domElement += "</td>";
			domElement += "</tr>";
			return domElement;			
		},
		elementProperty:function(singular,current_index,template_index,header){
			domElement = "";
			domElement += "<div class='row accredify_"+singular+"_"+current_index+" accredify_"+singular+"' data-accredify-"+singular+"='"+current_index+"'>";
			
			//Remove Button
			domElement += "<div class='col-sm-1 pull-right'>";			
				domElement += "<img class='accredify_"+singular+"_"+current_index+" accredify_"+singular+"_remove' src='https://s3.amazonaws.com/accredify-assets/whitelabel-api/ic_remove.png' style='cursor:pointer; width:20px;' title='Remove'>";
			domElement += "</div>";			

			//Header
			if(header != undefined){
				domElement += "<div class='col-sm-12'>";			
					domElement += "<h4>"+header+"</h4>";
				domElement += "</div>";			
			}

			
			var street_address = Accredify.DOM.templates[template_index].filterObjects("template_ident", "street_address")[0];
			var appt_suite = Accredify.DOM.templates[template_index].filterObjects("template_ident", "appt_suite")[0];
			var city = Accredify.DOM.templates[template_index].filterObjects("template_ident", "city")[0];
			var state = Accredify.DOM.templates[template_index].filterObjects("template_ident", "state")[0];
			var zip = Accredify.DOM.templates[template_index].filterObjects("template_ident", "zip")[0];
			var state = Accredify.DOM.templates[template_index].filterObjects("template_ident", "state")[0];
			var value = Accredify.DOM.templates[template_index].filterObjects("template_ident", "value")[0];
			var mortgage = Accredify.DOM.templates[template_index].filterObjects("template_ident", "mortgage")[0];
			var is_owned = Accredify.DOM.templates[template_index].filterObjects("template_ident", "is_owned")[0];

			//Street Address		
			domElement += "<div class='col-sm-12'>";
				domElement += "<label>"+street_address.label+"</label><br/>";							
				domElement += String(Accredify.DOM.inputBuilder(street_address)).replaceAll("xxx", current_index);
			domElement += "</div>";

			//Apartment/Suite
			var appt_elm_size = (is_owned == undefined)? 'col-sm-12' : 'col-sm-8';//Is the question of ownership asked? Adjust look.
			domElement += "<div class='"+appt_elm_size+"'>";
				domElement += "<label>"+appt_suite.label+"</label><br/>";										
				domElement += String(Accredify.DOM.inputBuilder(appt_suite)).replaceAll("xxx", current_index);
			domElement += "</div>";

			//Own or Rent?
			if(is_owned != undefined){
				domElement += "<div class='col-sm-4'>";
				domElement += "<label>"+is_owned.label+"</label><br/>";																		
					domElement += String(Accredify.DOM.inputBuilder(is_owned)).replaceAll("xxx", current_index);
				domElement += "</div>";
			}

			//City
			domElement += "<div class='col-sm-4'>";
				domElement += "<label>"+city.label+"</label><br/>";																					
				domElement += String(Accredify.DOM.inputBuilder(city)).replaceAll("xxx", current_index);
			domElement += "</div>";

			//State
			domElement += "<div class='col-sm-4'>";
				domElement += "<label>"+state.label+"</label><br/>";																								
				domElement += String(Accredify.DOM.inputBuilder(state)).replaceAll("xxx", current_index);
			domElement += "</div>";

			//Zip
			domElement += "<div class='col-sm-4'>";
				domElement += "<label>"+zip.label+"</label><br/>";																											
				domElement += String(Accredify.DOM.inputBuilder(zip)).replaceAll("xxx", current_index);
			domElement += "</div>";


			///Property Value, if it's part of the template it's required
			if(mortgage != undefined){
				domElement += "<div class='col-sm-6'>";
				domElement += "<label>"+value.label+"</label><br/>";																															
					domElement += String(Accredify.DOM.inputBuilder(value)).replaceAll("xxx", current_index);
				domElement += "</div>";
			}

			///Mortgage, if it's part of the template it's required
			if(mortgage != undefined){
				domElement += "<div class='col-sm-6'>";
					domElement += "<label>"+mortgage.label+"</label><br/>";																																			
					domElement += String(Accredify.DOM.inputBuilder(mortgage)).replaceAll("xxx", current_index);
				domElement += "</div>";
			}			
			
			domElement += "</div>";
			return domElement;			
		}				
	},//DOM

	//Helper Functions For Assets Verification
	Assets:{
		sum:0,
		init:function(){
			//TODO look into amount.
			$("#accredify_assets").attr("data-accredify-amount",0);//Asset Counter
			$("#accredify_assets").attr("data-accredify-index",0);//Asset index, used when adding new assets
			$("#accredify_liabilities").attr("data-accredify-amount",0);//Liabilities Counter
			$("#accredify_liabilities").attr("data-accredify-index",0);//Liabilities index, used when adding new liabilities
			$("#accredify_properties").attr("data-accredify-amount",0);//Properties Counter
			$("#accredify_properties").attr("data-accredify-index",0);//Properties index, used when adding new properties

		},
		add:function(container,template_index){		
			var type,singular;

			//Types of Assets
			switch(template_index){
				case 'assets':
					type = 'assets';
					singular = 'asset';					
				break;
				case 'liabilities':
					type = 'liabilities';
					singular = 'liability';					
				break;
				case 'properties_w_value':
					type = 'properties';
					singular = 'property';					
				break;
			}

			var current_index = parseInt($("#accredify_"+type).attr("data-accredify-index"));//Current Index
			$("#accredify_"+type).attr("data-accredify-index",current_index+1);//Increment Index
			
         			var domElement = "";
         			if(type == "properties"){
         				domElement = Accredify.DOM.elementProperty(singular,current_index,template_index,'Additional Property');         				
         			}else{
         				domElement = Accredify.DOM.elementAssetLiabilityRow(singular,current_index,template_index);         				
         			}
			$(container).append(domElement);//Append DOM Element

			//Logic for removing asset when remove icon clicked
			$(".accredify_"+singular+"_"+current_index+".accredify_"+singular+"_remove").click(function(){
				Accredify.DOM.remove($('.accredify_'+singular+'_'+current_index));
				Accredify.Validator.reset();//Add Verification Logic
				Accredify.Assets.update();//Recalculate Assets
			});

			Accredify.Validator.reset();//Add Verification Logic
			Accredify.addMasks();//Mask Inputs
			Accredify.Assets.updater(".accredify_"+singular+"_"+current_index,template_index);//Updates Asset Count on field change
		},
		updater:function(target,template_index){
			switch(template_index){
				case 'assets':
					$(target).find(".accredify_assets_value").change(function(){Accredify.Assets.update();})
				break;
				case 'liabilities':
					$(target).find(".accredify_iabilities_value").change(function(){Accredify.Assets.update();})				
				break;
				case 'properties_w_value':
					$(target).find(".accredify_properties_value").change(function(){Accredify.Assets.update();})								
					$(target).find(".accredify_properties_mortgage").change(function(){Accredify.Assets.update();})								
                                        $(target).find(".accredify_properties_is_owned").change(function(){Accredify.Assets.update();})
				break;
			}		
		},		
		update:function(){
			var asset_sum = 0;

			//Sum Assets
			$(".accredify_assets_value").each(function(i,v){ 
				var num = parseInt($(v).val().replace(/[^\/\d]/g,''));
				if(!isNaN(num)){
					asset_sum +=  num;
				}
			});

			//Sum Liabilities
			$(".accredify_iabilities_value").each(function(i,v){ 
				var num = parseInt($(v).val().replace(/[^\/\d]/g,''));
				if(!isNaN(num)){
					asset_sum -=  num;
				}
			});

			//Sum Property Value
			$(".accredify_property").each(function(i,v){
				
				var value = parseInt($(v).find(".accredify_properties_value").val().replace(/[^\/\d]/g,''));
				var mortgage = parseInt($(v).find(".accredify_properties_mortgage").val().replace(/[^\/\d]/g,''));
			        var is_owned = $(v).find(".accredify_properties_is_owned").val();;
				
				if(!isNaN(value) && is_owned == "Own"){

					if( (isNaN(mortgage))){
						$(v).find(".accredify_properties_mortgage").val(0);
						mortgage = 0;
					}

					asset_sum +=  value-mortgage;					
				}				
			});

			Accredify.Assets.sum = asset_sum;//Update Assets Count

			if($("#accredify_assets_counter").length != 0){
				$("#accredify_assets_counter").html("$"+parseInt(asset_sum).formatMoney(2, '.', ','));
			}

		}
	}//Assets		
}

Accredify.Dependencies.init();//Kick off Dependencies
})();
